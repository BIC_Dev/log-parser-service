# Log Parser Service

Parses Nitrado log files, caches Chat and Admin logs for searching, and normalizes the data for K/D (Player, Dino, and Structure) and Taming.