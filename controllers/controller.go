package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/BIC_Dev/log-parser-service/services/redis"
	"gitlab.com/BIC_Dev/log-parser-service/utils"
	"go.uber.org/zap"
)

// Controller struct containing DB access and Configs
type Controller struct {
	Config       *utils.Config
	Log          *zap.Logger
	ServiceToken string
	Cache        *redis.Cache
}

// SendJSONResponse sends a response to the client
func SendJSONResponse(w http.ResponseWriter, response interface{}, statusCode int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	json.NewEncoder(w).Encode(response)
}

// GetNitradoAuthToken from the controller struct
func (c *Controller) GetNitradoAuthToken(accountName string) (string, *redis.CacheError) {
	cacheKey := fmt.Sprintf("nitradotoken:%s", accountName)

	token, cErr := c.Cache.Get(cacheKey)

	if cErr != nil {
		return "", cErr
	}

	if token == "" {
		return "", &redis.CacheError{
			Err:     fmt.Errorf("Nitrado auth token is not in cache: %s", accountName),
			Message: "Could not retrieve a Nitrado Auth Token from cache",
		}
	}

	return token, nil
}
