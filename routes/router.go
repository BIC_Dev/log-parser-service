package routes

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/log-parser-service/controllers"
	"gitlab.com/BIC_Dev/log-parser-service/services/redis"
	"gitlab.com/BIC_Dev/log-parser-service/viewmodels"
	"go.uber.org/zap"
)

type middleware struct {
	Log          *zap.Logger
	ServiceToken string
	Cache        *redis.Cache
}

// GetRouter creates and returns a router
func GetRouter() *mux.Router {
	return mux.NewRouter().StrictSlash(true)
}

// AddRoutes adds all necessary routes to the router
func AddRoutes(router *mux.Router, c *controllers.Controller) {
	middleware := middleware{
		Log:          c.Log,
		ServiceToken: c.ServiceToken,
		Cache:        c.Cache,
	}

	router.HandleFunc("/nitrado-service-v2/status", c.GetStatus).Methods("GET")

	router.Use(middleware.loggingMiddleware)
	router.Use(middleware.authenticationMiddleware)
	router.Use(middleware.accountNameMiddleware)
}

// StartListener starts the HTTP listener
func StartListener(router *mux.Router, port string) {
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), router))
}

// Logs the request
func (m *middleware) loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.RequestURI == "/nitrado-service-v2/status" {
			next.ServeHTTP(w, r)
		} else {
			m.Log.Info("Handling request", zap.String("request_uri", r.RequestURI), zap.String("remote_address", r.RemoteAddr), zap.String("x_forwarded_for", r.Header.Get("x-forwarded-for")))
			next.ServeHTTP(w, r)
		}
	})
}

// Verifies the Service-Token header is set and authorized for access to the API
func (m *middleware) authenticationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		serviceTokenHeader := r.Header.Get("Service-Token")
		if r.RequestURI == "/nitrado-service-v2/status" {
			next.ServeHTTP(w, r)
		} else if serviceTokenHeader == "" {
			m.Log.Warn("Missing Service-Token header", zap.String("request_uri", r.RequestURI), zap.String("remote_address", r.RemoteAddr), zap.String("x_forwarded_for", r.Header.Get("x-forwarded-for")))
			controllers.SendJSONResponse(w, viewmodels.ErrorResponse{
				StatusCode: http.StatusUnauthorized,
				Error:      "Missing Service-Token header",
				Message:    "A Service-Token header must be set for all routes",
			}, http.StatusUnauthorized)
		} else if serviceTokenHeader == m.ServiceToken {
			next.ServeHTTP(w, r)
		} else {
			m.Log.Warn("Invalid Service-Token header", zap.String("request_uri", r.RequestURI), zap.String("remote_address", r.RemoteAddr), zap.String("x_forwarded_for", r.Header.Get("x-forwarded-for")))
			controllers.SendJSONResponse(w, viewmodels.ErrorResponse{
				StatusCode: http.StatusUnauthorized,
				Error:      "Invalid Service-Token header",
				Message:    "An invalid Service-Token header was sent with request",
			}, http.StatusUnauthorized)
		}
	})
}

// Verifies the Account-Name header is set
func (m *middleware) accountNameMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		accountNameHeader := r.Header.Get("Account-Name")
		if r.RequestURI == "/nitrado-service-v2/status" || r.RequestURI == "/nitrado-service-v2/update/tokens" {
			next.ServeHTTP(w, r)
		} else if accountNameHeader == "" {
			m.Log.Warn("Missing Account-Name header", zap.String("request_uri", r.RequestURI), zap.String("remote_address", r.RemoteAddr), zap.String("x_forwarded_for", r.Header.Get("x-forwarded-for")))
			controllers.SendJSONResponse(w, viewmodels.ErrorResponse{
				StatusCode: http.StatusUnauthorized,
				Error:      "Missing Account-Name header",
				Message:    "An Account-Name header must be set for all routes",
			}, http.StatusUnauthorized)
		} else {
			next.ServeHTTP(w, r)
		}
	})
}
