module gitlab.com/BIC_Dev/log-parser-service

go 1.14

require (
	github.com/gorilla/mux v1.8.0
	github.com/mediocregopher/radix/v3 v3.5.2
	go.uber.org/zap v1.16.0
	gopkg.in/yaml.v2 v2.3.0
)
