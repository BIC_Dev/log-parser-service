package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strconv"

	"gitlab.com/BIC_Dev/log-parser-service/controllers"
	"gitlab.com/BIC_Dev/log-parser-service/routes"
	"gitlab.com/BIC_Dev/log-parser-service/services/redis"
	"gitlab.com/BIC_Dev/log-parser-service/utils"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// Service information for the service
type Service struct {
	Config *utils.Config
	Log    *zap.Logger
}

func main() {
	service := InitService()
	cache := InitCache(service)
	controller := InitController(service, cache)

	AddNitradoTokensToCache(service, cache)

	InitRouter(service, controller)
}

// InitService initializes the service with env vars
func InitService() *Service {
	env, envExist := os.LookupEnv("ENVIRONMENT")
	logLevelString, llExist := os.LookupEnv("LOG_LEVEL")
	logLevel, llErr := strconv.Atoi(logLevelString)

	if envExist == false {
		log.Fatal("Missing ENVIRONMENT environment variable")
	}

	if llExist == false {
		log.Fatal("Missing LOG_LEVEL environment variable")
	}

	if llErr != nil {
		log.Fatal("Unable to convert LOG_LEVEL to int")
	}

	logger := InitLogger(logLevel)

	config := utils.GetConfig(env)

	return &Service{
		Config: config,
		Log:    logger,
	}
}

// InitLogger intitializes the logger
func InitLogger(logLevel int) *zap.Logger {
	cfg := zap.Config{
		Encoding:         "json",
		Level:            zap.NewAtomicLevelAt(zapcore.InfoLevel),
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
		EncoderConfig: zapcore.EncoderConfig{
			MessageKey: "message",

			LevelKey:    "level",
			EncodeLevel: zapcore.CapitalLevelEncoder,

			TimeKey:    "time",
			EncodeTime: zapcore.ISO8601TimeEncoder,

			CallerKey:    "caller",
			EncodeCaller: zapcore.ShortCallerEncoder,
		},
	}

	logger, err := cfg.Build()

	if err != nil {
		log.Fatalf("Failed to build zap logger: %s", err.Error())
	}

	return logger
}

// InitCache initializes the Redis cache
func InitCache(service *Service) *redis.Cache {
	pool, cacheErr := redis.GetClient(service.Config.Redis.Host, service.Config.Redis.Port, service.Config.Redis.Pool)

	if cacheErr != nil {
		service.Log.Fatal(cacheErr.Message, zap.Error(cacheErr))
	}

	return &redis.Cache{
		Client: pool,
		Prefix: service.Config.Redis.Prefix,
	}
}

// InitController intiializes the Controller struct
func InitController(service *Service, cache *redis.Cache) *controllers.Controller {
	serviceToken, stExist := os.LookupEnv("SERVICE_TOKEN")

	if !stExist {
		service.Log.Fatal("Missing SERVICE_TOKEN environment variable")
	}

	return &controllers.Controller{
		Config:       service.Config,
		Log:          service.Log,
		ServiceToken: serviceToken,
		Cache:        cache,
	}
}

// AddNitradoTokensToCache adds the Nitrado tokens to Redis cache
func AddNitradoTokensToCache(s *Service, c *redis.Cache) {
	accountsJSON, ajExists := os.LookupEnv("NITRADO_TOKENS")

	if !ajExists {
		s.Log.Fatal("Missing Nitrado account credentials")
	}

	var nitradoAccounts map[string]string

	unmarshalErr := json.Unmarshal([]byte(accountsJSON), &nitradoAccounts)

	if unmarshalErr != nil {
		s.Log.Fatal("Could not unmarshal nitrado accounts", zap.Error(unmarshalErr))
	}

	for k, v := range nitradoAccounts {
		cacheKey := fmt.Sprintf("loggernitradotoken:%s", k)

		cErr := c.Set(cacheKey, v, "")

		if cErr != nil {
			s.Log.Error("Failed to add Nitrado token to cache", zap.Error(cErr), zap.String("nitrado_token_name", k))
		}

		s.Log.Info("Added Nitrado token to cache", zap.String("nitrado_token_name", k))
	}
}

// InitRouter intitializes the router and starts the listener
func InitRouter(s *Service, c *controllers.Controller) {
	lp, lpExist := os.LookupEnv("LISTENER_PORT")

	if !lpExist {
		s.Log.Fatal("Missing LISTENER_PORT environment variable")
	}

	router := routes.GetRouter()
	routes.AddRoutes(router, c)

	s.Log.Info("Set up router")

	s.Log.Info("Starting MUX listener", zap.String("listener_port", lp))
	routes.StartListener(router, lp)
}
