package redis

// CacheError cache error struct
type CacheError struct {
	Err     error
	Message string
}

func (r *CacheError) Error() string {
	return r.Err.Error()
}

// GetMessage details of the error
func (r *CacheError) GetMessage() string {
	return r.Message
}

// SetMessage sets the details map of the error
func (r *CacheError) SetMessage(message string) {
	r.Message = message
}
