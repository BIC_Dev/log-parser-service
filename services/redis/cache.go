package redis

import (
	"fmt"

	"github.com/mediocregopher/radix/v3"
)

// Cache struct
type Cache struct {
	Client *radix.Pool
	Prefix string
}

// GetClient instantiates and returns a connection pool
func GetClient(host string, port int, poolSize int) (*radix.Pool, *CacheError) {
	hostWithPort := fmt.Sprintf("%s:%d", host, port)
	pool, err := radix.NewPool("tcp", hostWithPort, poolSize)

	if err != nil {
		return nil, &CacheError{
			Err:     err,
			Message: "Unable to create a new cache pool",
		}
	}

	return pool, nil
}

// Get a value by key
func (c *Cache) Get(key string) (string, *CacheError) {
	key = fmt.Sprintf("%s:%s", c.Prefix, key)
	var getVal string
	err := c.Client.Do(radix.Cmd(&getVal, "GET", key))

	if err != nil {
		return "", &CacheError{
			Err:     err,
			Message: fmt.Sprintf("Unable to get value from Redis for key: %s", key),
		}
	}

	return getVal, nil
}

// Set a key:value pair
func (c *Cache) Set(key, value string, ttl string) *CacheError {
	key = fmt.Sprintf("%s:%s", c.Prefix, key)
	err := c.Client.Do(radix.Cmd(nil, "SET", key, value))

	if err != nil {
		return &CacheError{
			Err:     err,
			Message: "Unable to set key:value pair in Redis",
		}
	}

	if ttl != "" {
		exErr := c.Client.Do(radix.Cmd(nil, "EXPIRE", key, ttl))

		if exErr != nil {
			return &CacheError{
				Err:     exErr,
				Message: "Unable to set TTL for key",
			}
		}
	}

	return nil
}
